# DemoButton 按钮

### 介绍

DemoButton 是一个示例按钮组件

### 引入

```js
import Vue from 'vue';
import { DemoButton } from 'mallui';

Vue.use(DemoButton);
```

## 代码演示

### 按钮类型

按钮支持 `default`、`primary`、`success` 三种类型，默认为 `default`。
```html
<lc-demo-button type="primary">主要按钮</lc-demo-button>
<lc-demo-button type="success">成功按钮</lc-demo-button>
<lc-demo-button type="default">默认按钮</lc-demo-button>
```
### 禁用状态

通过 `disabled` 属性来禁用按钮，禁用状态下按钮不可点击。

```html
<lc-demo-button disabled type="primary">禁用状态</lc-demo-button>
<lc-demo-button disabled type="primary">禁用状态</lc-demo-button>
```

## API

### Props

| 参数          | 说明     | 类型     | 可选值            | 默认值    |
| ------------- | -------- | -------- | ----------------- | --------- |
| type          | 按钮类型 | _string_ | primary/success | `primary` |
| disabled | 是否禁用按钮 | _boolean_ | -                 | `false`         |

### Events

| 事件名 | 说明       | 回调参数            |
| ------ | ---------- | ------------------- |
| click  | 点击时触发 | _event: MouseEvent_ |

### Slots

| 名称    | 说明     |
| ------- | -------- |
| default | 默认插槽 |
