# ProductAnalysisTop 按钮

### 介绍

ProductAnalysisTop 是产品分析详情页，头部基础信息组件

### 引入

```js
import Vue from 'vue';
import { ProductAnalysisTop } from 'mallui';

Vue.use(ProductAnalysisTop);
```

## 代码演示

### 基础用法

```html
<lc-product-analysis-top :prodDetailsRes="prodDetailsRes"><lc-product-analysis-top>
```

```js
export default {
  data () {
    return {
      prodDetailsRes: null
    }
  },
  created () {
    this.$http.Get('/eplus/api/prod/get/prodDetails.json', { prodCode: 'S78611' }).then(res => {
      this.prodDetailsRes = res.data
    })
  }
}
```

## API

### Interface

- T01 -- api/prod/get/prodDetails.json  查询产品详情

| 参数     | 说明     | 类型     | 默认值 |
| -------- | -------- | -------- | ------ |
| prodCode | 产品代码 | _string_ | -      |

### Props

| 参数           | 说明                          | 类型   | 默认值 |
| -------------- | ----------------------------- | ------ | ------ |
| prodDetailsRes | 产品详情，Interface-T01出参 | _Object_ | -      |
