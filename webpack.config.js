/**
 * vant-cli 开发环境及组件示例 Webpack 配置
 */
const path = require('path');
const webpack = require('webpack');
const config = require('./build/config');
const {getRawArgv} = require('./build/util')
// 不同模式的环境变量
const envConf = require('./config/'+getRawArgv('mode', 'dev', 'prd')+'.env')

module.exports = function () {
  return {
    entry: {
      // 移动端模拟组件入口修改
      // 'site-mobile': [path.join(__dirname, './docs/site/entry.js')]
      'site-mobile': ['./docs/site/entry.js'],
      // 'site-desktop': ['./docs/site/entry'],
    },
    // 开发中配置
    devServer: {
      host: '0.0.0.0',
      port: '8085',
      hot: true
    },
    // 不需要生成 source map
    devtool: false,
    // 设置模块如何被解析
    resolve: {
      // 别名
      alias: config.alias
    },
    plugins: [
      // 环境变量
      new webpack.DefinePlugin({
        'process.env': JSON.stringify(envConf)
      })
    ],
    // 模块处理规则
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(vue|js)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'eslint-loader',
              options: {
                emitWarning: true,
                formatter: require('eslint-friendly-formatter'),
                fix: true
              }
            }
          ]
        },
        {
          // 图片、字体文件配置cdn
          test: /\.(svg|otf|ttf|woff2?|eot|gif|png|jpe?g)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                publicPath: envConf.MALL_CDN_URL,
                name: '[path][name].[ext]',
                esModule: false
              }
            }
          ]
        }
      ]
    }
  }
}
