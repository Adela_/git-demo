import axios from 'axios'
import qs from 'qs'

// axios 配置
axios.defaults.withCredentials = true
axios.defaults.timeout = 10000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'

// 添加响应拦截器
axios.interceptors.response.use(response => {
  if (response.data && response.data.success === false) {
    // 接口异常
    return Promise.reject(response.data)
  }
  return response
}, err => {
  if (err && err.response) {
    switch (err.response.status) {
      case 502:
        err.message = '网关错误,当前网络有误，请稍后再试。'
        break
      case 0:
        err.message = '网络请求超时，请稍后再试。'
        break
    }
  }
  if (err.message.search('timeout') !== -1) {
    err.message = '请求超时，请保持良好的网络环境。'
  }
  return Promise.reject(err)
})

const Post = function Post (url, ...params) {
  console.log(params)
  const data = params.length > 0 ? params[0] : {}
  return axios.post(process.env.MALL_API_HOST + url, qs.stringify(data))
}

const Get = function Get (url, ...params) {
  console.log(params, params.length)
  const data = params.length > 0 ? params[0] : {}
  return axios.get(process.env.MALL_API_HOST + url, { params: data })
}

export default { Get, Post }
