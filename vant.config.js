/**
 * `vant-cli`的打包配置和文档站点配置
 */
// 网站目录配置
const siteNav = require('./siteNav.json')

module.exports = {
  // 组件库名称
  name: 'mallui',
  // 构建配置
  build: {
    // CSS 预处理器配置
    css: {
      preprocessor: 'less'
    },
    // webpack 的`output.publicPath`
    site: {
      publicPath: './'
    },
    // 通过 `export * from 'xxx'` 导出组件内部的所有模块、类型定义
    namedExport: true
  },
  vetur: {
    tagPrefix: 'lc-'
  },
  // 开发文档与示例站点导航配置
  site: {
    // 标题
    title: '理财频道UI组件库',
    // 站点logo
    logo: '',
    // 文档站点的左侧导航，数组中的每个对象表示一个导航分组
    nav: siteNav
  }
}
