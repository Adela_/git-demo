# 开发指南

### 参与开发

```bash
# 安装依赖
npm install

# 进入开发模式，浏览器访问 http://localhost:8085
npm run dev

# 构建文档站点，在 `site` 目录生成可用于生产环境的文档站点代码
npm run build-site

# 构建组件库。运行 build 命令会在 `lib` 目录下生成可用于生产环境的组件代码
npm run build

# 构建组件库。运行 build-cli 命令会在 `lib` 目录下生成可用于生产环境的组件代码,该命令支持命令行传参
npm run build-cli

# 清空与创建 `lib` 目录
npm run clean

# 构建按需引入组件库
npm run build-component

# 生成webpack.component入口文件
npm run build-component-entry

# 生成webpack.conf入口文件
npm build-entry

# 构建组件库(所有组件)
npm build-lib

# babel 编码assets目录下的文件，用于按需引入
npm build-utils

```

> 命令行传参:   
> npm run dev、npm run build-site、npm run build-cli 支持命令行传参，格式 -- mode=uat key=value  
> 使用mode参数来设置全局环境变量：  
> 如 npm run dev -- mode=uat 表示把config/uat.env.js中的k-v设为全局变量  
> npm run dev 默认模式为“dev”，npm run build-site、npm run build-cli默认模式为“prd”
> 

### 添加新组件

添加新组件时，请按照下面的目录结构组织文件，并在 `siteNav.json` 和 `components.json` 中配置组件名称、入口等信息。

```
src
└─ button
   ├─ demo             # 示例代码
   │   └─ index.vue    # 组件示例
   ├─ index.js         # 组件入口，可运行npm run build-component-entry自动生成
   ├─ index.less       # 组件样式
   ├─ index.vue        # 组件源码
   └─ README.md        # 组件文档
```

### 开发规范

- 按照目录结构规范添加文件
- 示例组件中可以使用 this.$http 发送 GET、POST请求，接口为理财频道API
- 样式单位 px
- 需要接口数据的组件，prop建议接受整个接口响应数据response.data。方便调用方仅需处理接口调用逻辑
- 
