# 快速上手

### 安装

```bash
# 推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。
npm i mallui -S

# 通过 yarn 安装
yarn add mallui
```
### 通过 CDN 安装
可以直接在 html 文件中引入 CDN 链接，之后你可以通过全局变量 `MALLUI` 访问到所有组件。

```html
<!-- 引入样式文件 -->
<link rel="stylesheet" href="/lib/styles/mallui.css"/>

<!-- 引入 Vue 和 mallui 的 JS 文件 -->
<script src="https://apicdn.app.gtja.com/mall/eplus/static/js/vue.js"></script>
<script src="/lib/mallui.js"></script>

<!-- 以下为html文件示例 -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/lib/styles/mallui.css">
  </head>
  <body>
    <div id="index">
      <lc-demo-button color="#03a9f4" style="margin-left: 15px">按钮</lc-demo-button>
    </div>
  </body>
  <script src="https://apicdn.app.gtja.com/mall/eplus/static/js/vue.js"></script>
  <script src="/lib/mallui.js"></script>
  <script>
    new Vue({ el: '#index' })
  </script>
</html>
```

## 引入组件  
你可以引入整个 mallui，或是根据需要仅引入部分组件。   

### 方式一. 完整引入

在 main.js 中写入以下内容：

```javascript
import Vue from 'vue';
import mallui from 'mallui'
import 'mallui/lib/styles/mallui.css'
import App from './App.vue';

Vue.use(mallui);

new Vue({
  el: '#app',
  render: h => h(App)
});
```

以上代码便完成了 mallui 的引入。需要注意的是，样式文件需要单独引入。

### 方式二. 通过 babel 插件按需引入组件

[babel-plugin-import](https://github.com/ant-design/babel-plugin-import) 是一款 babel 插件，它会在编译过程中将 import 语句自动转换为按需引入的方式。

```bash
# 安装插件
npm i babel-plugin-import -D
```

在.babelrc 或 babel.config.js 中添加配置：

```json
{
  "presets": [["es2015", { "modules": false }]],
  "plugins": [
    [
      "component",
      {
        "libraryName": "mallui",
        "libraryDirectory": "styles"
      }
    ]
  ]
}
```
接下来，如果你只希望引入部分组件，比如 Button，那么需要在 main.js 中写入以下内容，插件会自动将代码转化为按需引入的形式：

```js
import Vue from 'vue';
import { DemoButton } from 'mallui';
import App from './App.vue';

Vue.component(DemoButton.name, DemoButton);
/* 或写为
 * Vue.use(DemoButton)
 */

new Vue({
  el: '#app',
  render: h => h(App)
});
```

### 方式三. 手动按需引入组件

在不使用插件的情况下，可以手动引入需要使用的组件和样式。

```js
// 引入组件
import lcDemoButton from 'mallui/lib/demo-button';
// 引入基础样式
import 'mallui/lib/styles/base.css'
// 引入组件对应的样式，若组件没有样式文件，则无须引入
import 'mallui/lib/styles/demo-button.css';
// 使用组件
<lc-demo-button type="primary">按钮</lc-demo-button>
```
