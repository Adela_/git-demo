# 目录结构

## 源代码目录

组件库的基本目录结构如下所示：
- 组件代码位于 src 下，每个组件一个文件夹
- docs 目录下是文档网站的代码，本地开发时可以在目录下运行 npm run dev 开启文档网站

```
project
├─ assets               # 组件通用js,按需引入，babel打包处理
│   ├─ mixins               # 组件混入
│   └─ utils                # 工具类
│
├─ build                # 打包配置项
│   ├─ config               # 配置项通用变量
│   ├─ generate-entry       # webpack.common、webpack.conf入口文件模板生成配置
│   ├─ generate-component-entry     # 单组件入口文件模板生成配置
│   ├─ util                 # 配置项通用方法
│   ├─ webpack.base         # webpack通用配置
│   ├─ webpack.common       # mallui.common.js webpack配置
│   ├─ webpack.component    # 单组件按需引入，webpack配置
│   └─ webpack.conf         # 所有组件的入口，webpack配置
│
├─ config               # 不同模式的环境变量
│   ├─ dev                  # 本地开发
│   ├─ prd                  # 生产
│   ├─ pre                  # 预生产
│   └─ uat                  # 测试
│
├─ docs                 # 开发示例站点静态文档目录
│   ├─ assets               # 样式等修改
│   ├─ markdown             # 静态文档
│   │   ├─ directory.md         # 项目目录
│   │   ├─ home.md              # 文档首页
│   │   ├─ quickstart.md        # 快速上手
│   │   └─ ....        
│   └─ site                 # 站点修改
│       └─ entry.js             # site pc模式的入口
│
├─ plugins              # 插件配置
│   └─ axios                # 组件示例，axios配置
│
├─ src                  # 组件源代码
│   ├─ button               # button 组件源代码
│   ├─ dialog               # dialog 组件源代码
│   ├─ style                # 组件通用样式
│   │   ├─ base.less            # 基础，无需引入
│   │   └─ mixins               # 组件通用样式，按需引
│   │
│   ├─ entry-common         # generate-entry生成的commonjs2入口
│   └─ entry-index          # generate-entry生成的umd入口
│
├─ static               # 组件静态文件
│   ├─ fonts                # 字体
│   └─ images               # 图片
│
├─ babel.config.js              # Babel 配置文件
├─ babel.utils.config.json      # Babel 处理assets目录配置文件
├─ components.json              # 组件清单
├─ siteNav.json                 # 示例站点路由配置
├─ vant.config.js               # Vant Cli 配置文件
├─ webpack.config.js            # 示例站点webpack配置文件
├─ package.json
└─ README.md
```

单个组件的目录如下：

```
button
├─ demo             # 示例目录
│   └─ index.vue        # 组件示例
├─ index.js         # 组件入口，可运行npm run build-component-entry自动生成
├─ index.less       # 组件样式
├─ index.vue        # 组件源码
└─ README.md        # 组件文档
```

采用这种目录结构时，组件的使用者需要分别引入 JS 和 CSS 文件，也可以通过 babel-plugin-import 自动引入样式。

## 构建结果目录

运行 build 命令会在 `lib` 目录下生成可用于生产环境的组件代码，结构如下：

```
project
└─ lib                  # lib 目录下的代码遵循 commonjs 规范
    ├─ styles           # 编译后的样式文件
    │   ├─ button.css       # 按需引入编译后的样式
    │   └─ mallui.css       # 组件编译后的 CSS 文件
    ├─ utils            # assets目录babel后的代码目录
    ├─ mallui.js        # umd模式，引入所有组件的入口
    ├─ mallui.common.js # commonjs2模式，配置多入口
    └─ button.js        # 组件编译后的 JS 文件
```

> Tips: 'button'代指单个组件
