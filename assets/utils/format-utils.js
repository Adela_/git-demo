/**
 * 日期处理函数
 * @param {String} val 日期，格式yyyyMMdd
 * @param {String} join 日期格式化的连接符，如'-'
 * @param {Boolean} keepYear 日期格式化是否保留年份，true保留，默认是false
 */
export const dateKeep = function (val, join, keepYear = false) {
  return (keepYear ? val.substr(0, 4) + join : '') + val.substr(4, 2) + join + val.substr(6, 2)
}
/**
 * 浮点数处理
 * @param {String,Number} value 待处理数字
 * @param {Number} places 保留位数
 * @param {any} defalutValue 非数字时默认值，默认为'--'
 * @param {Boolean} keepPositiveSign 数字为正是否添加正号，默认不处理
 */
export const decimalkeep = function (value, places, defalutValue = '--', keepPositiveSign = false) {
  if (isNaN(value) || String(value).trim() === '') {
    return defalutValue
  }
  value = parseFloat(value).toFixed(places)
  if (value > 0 && keepPositiveSign) {
    return '+' + value
  }
  return value
}
