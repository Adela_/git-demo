const {getRawArgv} = require('./util')
const webpack = require('webpack');

class Service {
  constructor (context) {
    this.initialized = false
    this.context = context
  }

  async run () {
    process.env.NODE_ENV = 'production'
    this.initialized = true
    getRawArgv('mode', 'pre', 'prd')
    // 打包单组件
    await build(require('./webpack.component'));
    // 全部组件
    await build(require('./webpack.conf'))
    // 按需引入
    await build(require('./webpack.common'))
  }

}

function build(config) {
  return new Promise((resolve, reject) => {
    webpack(config, (err, stats) => {
      if (err || stats.hasErrors()) {
        reject();
      } else {
        resolve();
      }
    });
  });
}

const service = new Service(process.cwd())
service.run().catch(err => {
  console.error(err)
  process.exit(1)
})
