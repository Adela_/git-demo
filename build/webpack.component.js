const path = require('path');
const Components = require('../components.json');
const config = require('./config');
const webpackMerge = require("webpack-merge");
const webpackBase = require("./webpack.base.js");

const webpackConfig = webpackMerge(webpackBase, {
  entry: Components,
  output: {
    path: path.resolve(process.cwd(), './lib'),
    filename: '[name].js',
    chunkFilename: '[id].js',
    libraryTarget: 'commonjs2'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: config.alias,
    modules: ['node_modules']
  },
  externals: config.externals
});

module.exports = webpackConfig;
