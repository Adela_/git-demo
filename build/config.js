var path = require('path');
var fs = require('fs');
var Components = require('../components.json');

var utilsList = fs.readdirSync(path.resolve(__dirname, '../assets/utils'));
var mixinsList = fs.readdirSync(path.resolve(__dirname, '../assets/mixins'));
var externals = {};

Object.keys(Components).forEach(function(key) {
  externals[`mallui/src/${key}`] = `mallui/lib/${key}`;
});
utilsList.forEach(function(file) {
  file = path.basename(file, '.js');
  externals[`mallui/assets/utils/${file}`] = `mallui/lib/utils/${file}`;
});
mixinsList.forEach(function(file) {
  file = path.basename(file, '.js');
  externals[`mallui/assets/mixins/${file}`] = `mallui/lib/mixins/${file}`;
});

externals = [Object.assign({
  vue: 'vue'
}, externals)];

// 按需引入组件，外部扩展
exports.externals = externals;

// 别名
exports.alias = {
  static: path.resolve(__dirname, '../static'),
  plugins: path.resolve(__dirname, '../plugins'),
  mallui: path.resolve(__dirname, '../')
};

// 兼容不同环境下的vue别名
exports.vue = {
  root: 'Vue',
  commonjs: 'vue',
  commonjs2: 'vue',
  amd: 'vue'
};

exports.jsexclude = /node_modules/;
