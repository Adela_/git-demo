const path = require('path');
const config = require('./config');
const webpackMerge = require("webpack-merge");
const webpackBase = require("./webpack.base.js");

module.exports = webpackMerge(webpackBase, {
  entry: {
    base: ['./src/entry-common.js']
  },
  output: {
    path: path.resolve(process.cwd(), './lib'),
    filename: 'mallui.common.js',
    chunkFilename: '[id].js',
    libraryExport: 'default',
    library: 'MALLUI',
    libraryTarget: 'commonjs2'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: config.alias,
    modules: ['node_modules']
  },
  externals: config.externals
});
