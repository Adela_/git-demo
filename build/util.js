
/**
 * 格式化命令行传参，当命令行传值key=value时，处理格式为{key:value}
 */
const argvs = () => {
  const rawArgv = process.argv.slice(2)
  var argv = { _ : [] };
  for (var i = 0; i < rawArgv.length; i++) {
    var arg = rawArgv[i];
    if (/^[A-z0-9]+=/.test(arg)) {
      var letters = arg.split('=')
      argv[letters[0]] = letters[1]
    } else {
      argv._.push(arg);
    }
  }
  return argv
}
/**
 * 获取命令行中设定参数值
 * @param {string} name 参数key
 * @param {string} devDefault 测试环境默认值
 * @param {string} prdDefault 生产环境默认值
 */
const getRawArgv = (name, devDefault, prdDefault) => {
  var args = argvs();
  var value = args[name] ? args[name] : process.env.NODE_ENV === 'development' ? devDefault : prdDefault
  if (name === 'mode') {
    process.env.MALLUI_RUN_MODE = value
  }
  return value
}

module.exports = {
  getRawArgv
}
