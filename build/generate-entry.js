var Components = require('../components.json');
var fs = require('fs');
var render = require('json-templater/string');
var uppercamelcase = require('uppercamelcase');
var path = require('path');
var endOfLine = require('os').EOL;
// webpack.conf.js入口
var OUTPUT_PATH_CONF = path.join(__dirname, '../src/entry-index.js');
// webpack.common.js入口
var OUTPUT_PATH_COMMON = path.join(__dirname, '../src/entry-common.js');
var IMPORT_TEMPLATE = 'import {{name}} from \'../src/{{package}}/index.js\';';
var IMPORT_COMPONENT = 'import {{name}} from \'../src/{{package}}/index.vue\';';
var INSTALL_COMPONENT_TEMPLATE = '  {{name}}';
var SINGLE_COMPONENT_INSTALL_TEMPLATE = `
components.forEach(component => {
  component.install = function(Vue) {
    Vue.component(component.name, component);
  };
});
`;
var MAIN_TEMPLATE = `/* Automatically generated by './build/generate-entry.js' */

import '../src/style/base.less'
{{include}}

const components = [
{{install}}
];
{{singleCompomentInstall}}
const install = function(Vue) {
  components.forEach(component => {
    Vue.component(component.name, component);
  });
};

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export default {
  version: '{{version}}',
  install,
{{list}}
};
`;

var ComponentNames = Object.keys(Components);

var includeComponentTemplate_1 = [];
var includeComponentTemplate_2 = [];
var installTemplate = [];
var listTemplate = [];

ComponentNames.forEach(name => {
  var componentName = uppercamelcase(name);

  includeComponentTemplate_1.push(render(IMPORT_TEMPLATE, {
    name: componentName,
    package: name
  }));
  includeComponentTemplate_2.push(render(IMPORT_COMPONENT, {
    name: componentName,
    package: name
  }));
  installTemplate.push(render(INSTALL_COMPONENT_TEMPLATE, {
    name: componentName,
    component: name
  }));
  listTemplate.push(`  ${componentName}`);
});

var template_1 = render(MAIN_TEMPLATE, {
  include: includeComponentTemplate_1.join(endOfLine),
  singleCompomentInstall: '',
  install: installTemplate.join(',' + endOfLine),
  version: require('../package.json').version,
  list: listTemplate.join(',' + endOfLine)
});

var template_2 = render(MAIN_TEMPLATE, {
  include: includeComponentTemplate_2.join(endOfLine),
  singleCompomentInstall: SINGLE_COMPONENT_INSTALL_TEMPLATE,
  install: installTemplate.join(',' + endOfLine),
  version: require('../package.json').version,
  list: listTemplate.join(',' + endOfLine)
});

fs.writeFileSync(OUTPUT_PATH_CONF, template_1);
fs.writeFileSync(OUTPUT_PATH_COMMON, template_2);
console.log('[build entry] DONE:', OUTPUT_PATH_CONF);
console.log('[build entry] DONE:', OUTPUT_PATH_COMMON);

