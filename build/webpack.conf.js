const path = require('path');
const config = require('./config');
const webpackMerge = require("webpack-merge");
const webpackBase = require("./webpack.base.js");

module.exports = webpackMerge(webpackBase, {
  entry: {
    mallui: ['./src/entry-index.js']
  },
  output: {
    path: path.resolve(process.cwd(), './lib'),
    filename: 'mallui.js',
    chunkFilename: '[id].js',
    libraryTarget: 'umd',
    libraryExport: 'default',
    library: 'MALLUI',
    umdNamedDefine: true,
    globalObject: 'typeof self !== \'undefined\' ? self : this'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: config.alias
  },
  externals: {
    vue: config.vue
  }
});
