const webpack = require('webpack');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
// 当前运行的环境模式
const buildEnv = process.env.MALLUI_RUN_MODE ? process.env.MALLUI_RUN_MODE : 'prd';
const config = require('./config');
const envConf = require('../config/'+buildEnv+'.env.js');
const constant_1 = require("@vant/cli/lib/common/constant");
console.log('buildEnv', buildEnv, process.env.MALLUI_RUN_MODE, process.env.NODE_ENV)
const CSS_LOADERS = [
  {
    loader: MiniCssExtractPlugin.loader
  },
  'css-loader',
  {
    loader: 'postcss-loader',
    options: {
      config: {
        path: constant_1.POSTCSS_CONFIG_FILE,
      }
    }
  }
];

const webpackBaseConfig = {
  mode: 'production',
  performance: {
    hints: false
  },
  stats: 'errors-only',
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          output: {
            comments: false
          },
          compress: {
            drop_console: true
          }
        }
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.(js|ts|jsx|tsx)$/,
        include: process.cwd(),
        exclude: config.jsexclude,
        use: [
          {
            loader: 'cache-loader',
            options: {
              cacheDirectory: constant_1.CACHE_DIR,
            }
          },
          'babel-loader'
        ]
      },
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'vue-loader',
            options: {
              compilerOptions: {
                preserveWhitespace: false
              },
              transformToRequire: {
                video: ['src', 'poster'],
                source: 'src',
                img: 'src',
                image: 'xlink:href'
              }
            }
          }
        ]
      },
      {
        test: /\.less$/,
        use: [
          ...CSS_LOADERS,
          {
            loader: 'less-loader', options: {
              sourceMap: false
            }
          }
        ]
      },
      {
        test: /\.css$/,
        sideEffects: true,
        use: CSS_LOADERS
      },
      {
        test: /\.(svg|otf|ttf|woff2?|eot|gif|png|jpe?g)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: envConf.MALL_CDN_URL,
              name: '[path][name].[ext]',
              esModule: false,
              emitFile: false
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new ProgressBarPlugin(),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: "styles/[name].css",
      chunkFilename: "[id].css"
    }),
    new OptimizeCSSAssetsPlugin(),
    // 环境变量
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(envConf)
    })
  ]
};

module.exports = webpackBaseConfig;
